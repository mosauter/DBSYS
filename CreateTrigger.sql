DROP TABLE STORNIERUNGEN CASCADE CONSTRAINTS;

CREATE TABLE STORNIERUNGEN
(
BuchungsNr NUMBER not null,
Wohnungs_Name varchar(50) not null,
StartDatum DATE not null,
EndDatum DATE not null,
RechnungsNr Number not null,
BuchungsDatum DATE not null,
Kundenmail varchar(50) not null,
Endpreis NUMBER not null,
Stornierung DATE not null,
CONSTRAINT pk_Stornierungen PRIMARY KEY(BuchungsNr),
CONSTRAINT fk_Stornierungen_Kunde FOREIGN KEY(Kundenmail) REFERENCES Kunde(Mail),
CONSTRAINT fk_Stornierungen_Ferienwohnung FOREIGN KEY(Wohnungs_Name) REFERENCES Ferienwohnung(Wohnungs_Name)
);

COMMIT;

CREATE OR REPLACE TRIGGER BuchungStornieren
    BEFORE DELETE ON BUCHUNG
    FOR EACH ROW
    
BEGIN

INSERT INTO STORNIERUNGEN (BuchungsNr, Wohnungs_Name, StartDatum, EndDatum, RechnungsNr, BuchungsDatum, Kundenmail, Endpreis, Stornierung)
VALUES ( :old.BuchungsNr,
         :old.Wohnungs_Name,
         :old.StartDatum,
         :old.EndDatum,
         :old.RechnungsNr,
         :old.BuchungsDatum,
         :old.Kundenmail,
         :old.Endpreis,
         SYSDATE
       );
END;
/
COMMIT;

DELETE
FROM BUCHUNG
WHERE BUCHUNGSNR = (Select max(BUCHUNGSNR)
                    FROM BUCHUNG);
COMMIT;

DROP VIEW KUNDENSTATISTIK CASCADE CONSTRAINTS;
CREATE VIEW KUNDENSTATISTIK AS
    SELECT KUNDE.Mail, SumZahlungen.Summe, NVL(AnzBuchungen.AnzBuchungen, 0) AS AnzBuchungen, NVL(AnzStorno.AnzStorno, 0) AS AnzStorno
    FROM KUNDE
    LEFT OUTER JOIN (SELECT KUNDEnMail, Sum(Endpreis) AS Summe FROM BUCHUNG GROUP BY kundenMail HAVING Sum(Endpreis) > 0) SumZahlungen ON (SumZahlungen.kundenmail = Kunde.Mail)
    LEFT OUTER JOIN (SELECT kundenMail, count(*) AS AnzBuchungen FROM BUCHUNG GROUP BY kundenMail) AnzBuchungen ON (KUNDE.Mail = AnzBuchungen.kundenmail)
    LEFT OUTER JOIN (SELECT kundenMail, count(*) AS AnzStorno FROM STORNIERUNGEN GROUP BY kundenMail) AnzStorno ON (KUNDE.Mail = AnzStorno.kundenmail)
;
COMMIT;