DROP TABLE BUCHUNG CASCADE CONSTRAINTS;
DROP TABLE KUNDE CASCADE CONSTRAINTS;
DROP TABLE FERIENWOHNUNG CASCADE CONSTRAINTS;
DROP TABLE LAND CASCADE CONSTRAINTS;
DROP TABLE AUSSTATTUNG CASCADE CONSTRAINTS;
DROP TABLE TOURISTENATTRAKTION CASCADE CONSTRAINT;
DROP TABLE BILD CASCADE CONSTRAINT;
DROP TABLE AUSGESTATTET_IN CASCADE CONSTRAINT;
DROP TABLE IN_UMGEBUNG CASCADE CONSTRAINT;

CREATE TABLE LAND
(
Land_Name varchar(20),
CONSTRAINT pk_Land PRIMARY KEY(Land_Name)
);

CREATE TABLE KUNDE
(
Mail varchar(50) not null,
Vorname varchar(20) not null,
Nachname varchar(20) not null,
IBAN varchar(22) not null,
PLZ INTEGER not null,
Ort varchar(20) not null,
Strasse varchar(30) not null,
HNummer INTEGER not null,
Passwort varchar(20) not null,
Land_Name varchar(20) not null,
CONSTRAINT pk_Kunde PRIMARY KEY(Mail),
CONSTRAINT fk_Kunde_Land FOREIGN KEY(Land_Name) REFERENCES Land(Land_Name)
);

CREATE TABLE FERIENWOHNUNG
(
Wohnungs_Name varchar(50) not null,
Groesse NUMBER not null,
Tagespreis NUMBER not null,
Zimmer INTEGER not null,
PLZ INTEGER not null,
Ort varchar(20) not null,
Strasse varchar(20) not null,
HNummer varchar(20) not null,
Land_Name varchar(20) not null,
CONSTRAINT pk_Ferienwohnung PRIMARY KEY(Wohnungs_Name),
CONSTRAINT fk_Ferienwohnung_Land FOREIGN KEY(Land_Name) REFERENCES Land(Land_Name)
);

CREATE TABLE BILD
(
Link varchar(50) not null,
Wohnungs_Name varchar(50) not null,
CONSTRAINT pk_Bild PRIMARY KEY(Link),
CONSTRAINT fk_Bild_Wohnung FOREIGN KEY(Wohnungs_Name) REFERENCES Ferienwohnung(Wohnungs_Name)
);

CREATE TABLE BUCHUNG
(
BuchungsNr NUMBER GENERATED ALWAYS AS IDENTITY,
Wohnungs_Name varchar(50) not null,
StartDatum DATE not null,
EndDatum DATE not null,
RechnungsNr NUMBER not null,
BuchungsDatum DATE not null,
Kundenmail varchar(50) not null,
Endpreis NUMBER not null,
Zahlungseingang DATE,
Bewertung NUMBER(1) CHECK(Bewertung >0 AND Bewertung <11),
CONSTRAINT Zeitraum CHECK(EndDatum >= StartDatum + 3),
CONSTRAINT Bezahlt CHECK(Zahlungseingang < StartDatum),
CONSTRAINT pk_Buchung PRIMARY KEY(BuchungsNr),
CONSTRAINT fk_Buchung_Kunde FOREIGN KEY(Kundenmail) REFERENCES Kunde(Mail),
CONSTRAINT fk_Buchung_Ferienwohnung FOREIGN KEY(Wohnungs_Name) REFERENCES Ferienwohnung(Wohnungs_Name)
);

CREATE TABLE AUSSTATTUNG
(
Bezeichnung varchar(255) not null,
CONSTRAINT pk_Ausstattung PRIMARY KEY(Bezeichnung)
);

CREATE TABLE TOURISTENATTRAKTION
(
Bezeichnung varchar(30) not null,
Beschreibung varchar(255) not null,
CONSTRAINT pk_Touristenattraktion PRIMARY KEY(Bezeichnung)
);

CREATE TABLE AUSGESTATTET_IN
(
Ausstattung_Key NUMBER GENERATED ALWAYS AS IDENTITY,
Bezeichnung varchar(255) not null,
Wohnungs_Name varchar(50) not null,
CONSTRAINT fk_Ausgestattet_in_Ausstattung FOREIGN KEY(Bezeichnung) REFERENCES Ausstattung(Bezeichnung),
CONSTRAINT fk_Ausgestattet_in_Wohnung FOREIGN KEY(Wohnungs_Name) REFERENCES Ferienwohnung(Wohnungs_Name)
);

CREATE TABLE IN_UMGEBUNG
(
Umgebung_Key NUMBER GENERATED ALWAYS AS IDENTITY,
Bezeichnung varchar(30) not null,
Wohnungs_Name varchar(50) not null,
Entfernung NUMBER CHECK(Entfernung < 100),
CONSTRAINT fk_in_Umgebung_Touri FOREIGN KEY(Bezeichnung) REFERENCES Touristenattraktion(Bezeichnung),
CONSTRAINT fk_in_Umgebung_Ferienwohnung FOREIGN KEY(Wohnungs_Name) REFERENCES Ferienwohnung(Wohnungs_Name)
);

COMMIT;
