-- Laender
INSERT INTO LAND (Land_Name) VALUES ('Deutschland');
INSERT INTO LAND (Land_Name) VALUES ('Frankreich');
INSERT INTO LAND (Land_Name) VALUES ('Italien');
INSERT INTO LAND (Land_Name) VALUES ('Schweiz');
INSERT INTO LAND (Land_Name) VALUES ('Österreich');
INSERT INTO LAND (Land_Name) VALUES ('Spanien');

COMMIT;

-- Kunden
INSERT INTO KUNDE (Mail, Vorname, Nachname, IBAN, PLZ, Ort, Strasse, HNummer, Passwort, Land_Name) VALUES ('Max@Muster.de', 'Max', 'Muster', 'DE12345678901234567890', 88521, 'Ertingen', 'Daibersgaessle', 5, '12340987Muster', 'Deutschland');

INSERT INTO KUNDE (Mail, Vorname, Nachname, IBAN, PLZ, Ort, Strasse, HNummer, Passwort, Land_Name) VALUES ('Gundula@Gans.de', 'Gundula', 'Gans', 'DE12345678901234567890', 0815, 'Zuerich', 'Mustergasse', 7, '12340987Gans', 'Schweiz');

INSERT INTO KUNDE (Mail, Vorname, Nachname, IBAN, PLZ, Ort, Strasse, HNummer, Passwort, Land_Name) VALUES ('Karl@Sauter.de', 'Karl', 'Sauter', 'DE12345678901234567890', 123987, 'Wien', 'Badgasse', 9, '103928Sauter', 'Österreich');

INSERT INTO KUNDE (Mail, Vorname, Nachname, IBAN, PLZ, Ort, Strasse, HNummer, Passwort, Land_Name) VALUES ('italienlover@hotmail.com', 'Mister', 'Italiano', 'IT12345678901234567890', 74563,'la bachos', 'Italienerstraße', 2, 'lovaitaliano', 'Spanien');


COMMIT;

-- Ferienwohnungen
INSERT INTO FERIENWOHNUNG (Wohnungs_Name, Groesse, Tagespreis, Zimmer, PLZ, Ort, Strasse, HNummer, Land_Name) VALUES ('Gasthof Engel', 70.7, 213.99, 4.0, 82314, 'Hinterdupfingen', 'Hauptstrasse', 4, 'Deutschland');

INSERT INTO FERIENWOHNUNG (Wohnungs_Name, Groesse, Tagespreis, Zimmer, PLZ, Ort, Strasse, HNummer, Land_Name) VALUES ('La amore', 283.3, 84000.31, 10.5, 198235, 'Paris', 'Eifelturm', 3, 'Frankreich');

INSERT INTO FERIENWOHNUNG (Wohnungs_Name, Groesse, Tagespreis, Zimmer, PLZ, Ort, Strasse, HNummer, Land_Name) VALUES ('Gasthof Teufel', 30, 59.99, 1.5, 64783, 'Freiburg', 'Freiburgerstrasse', 17, 'Deutschland');

INSERT INTO FERIENWOHNUNG (Wohnungs_Name, Groesse, Tagespreis, Zimmer, PLZ, Ort, Strasse, HNummer, Land_Name) VALUES ('Hotel Rizzi', 56.9, 299.99, 5.5, 234837, 'Berlin', 'Karlsstrasse', 9, 'Deutschland');

INSERT INTO FERIENWOHNUNG (Wohnungs_Name, Groesse, Tagespreis, Zimmer, PLZ, Ort, Strasse, HNummer, Land_Name) VALUES ('El Torro', 23.5, 35.99, 2.0, 53254, 'Barcelona', 'Badstrasse', 19, 'Spanien');

INSERT INTO FERIENWOHNUNG (Wohnungs_Name, Groesse, Tagespreis, Zimmer, PLZ, Ort, Strasse, HNummer, Land_Name) VALUES ('El Barto', 32.4, 43.99, 3.5, 15235, 'Madrid', 'Calle de Velasquez', 8, 'Spanien');

INSERT INTO FERIENWOHNUNG (Wohnungs_Name, Groesse, Tagespreis, Zimmer, PLZ, Ort, Strasse, HNummer, Land_Name) VALUES ('Da Pizzo', 42.7, 56.23, 4.5, 85764, 'Roma di talia', 'Pizzastraße', 1, 'Italien');

COMMIT;

INSERT INTO AUSSTATTUNG (Bezeichnung) VALUES ('Whirlpool');
INSERT INTO AUSSTATTUNG (Bezeichnung) VALUES ('Sauna');
INSERT INTO AUSSTATTUNG (Bezeichnung) VALUES ('Balkon');
INSERT INTO AUSSTATTUNG (Bezeichnung) VALUES ('Tennisplatz');
INSERT INTO AUSSTATTUNG (Bezeichnung) VALUES ('Mini-Bar');
INSERT INTO AUSSTATTUNG (Bezeichnung) VALUES ('WLAN');

COMMIT;

INSERT INTO AUSGESTATTET_IN (Bezeichnung, Wohnungs_Name) VALUES ('Whirlpool', 'Hotel Rizzi');
INSERT INTO AUSGESTATTET_IN (Bezeichnung, Wohnungs_Name) VALUES ('Sauna', 'Hotel Rizzi');
INSERT INTO AUSGESTATTET_IN (Bezeichnung, Wohnungs_Name) VALUES ('Balkon', 'Hotel Rizzi');
INSERT INTO AUSGESTATTET_IN (Bezeichnung, Wohnungs_Name) VALUES ('Tennisplatz', 'Hotel Rizzi');
INSERT INTO AUSGESTATTET_IN (Bezeichnung, Wohnungs_Name) VALUES ('WLAN', 'Hotel Rizzi');
INSERT INTO AUSGESTATTET_IN (Bezeichnung, Wohnungs_Name) VALUES ('WLAN', 'El Barto');
INSERT INTO AUSGESTATTET_IN (Bezeichnung, Wohnungs_Name) VALUES ('WLAN', 'El Torro');
INSERT INTO AUSGESTATTET_IN (Bezeichnung, Wohnungs_Name) VALUES ('Tennisplatz', 'El Barto');
INSERT INTO AUSGESTATTET_IN (Bezeichnung, Wohnungs_Name) VALUES ('Mini-Bar', 'El Barto');
INSERT INTO AUSGESTATTET_IN (Bezeichnung, Wohnungs_Name) VALUES ('Sauna', 'Gasthof Engel');
INSERT INTO AUSGESTATTET_IN (Bezeichnung, Wohnungs_Name) VALUES ('Whirlpool', 'Gasthof Teufel');
INSERT INTO AUSGESTATTET_IN (Bezeichnung, Wohnungs_Name) VALUES ('Sauna', 'El Torro');
INSERT INTO AUSGESTATTET_IN (Bezeichnung, Wohnungs_Name) VALUES ('Sauna', 'El Barto');

COMMIT;

INSERT INTO TOURISTENATTRAKTION (Bezeichnung, Beschreibung) VALUES ('Tauchen', 'Freies Tauchen im Meer, sowohl als Anfänger als auch als Profi ein Muss');
INSERT INTO TOURISTENATTRAKTION (Bezeichnung, Beschreibung) VALUES ('Theater', 'Freier Eintritt in sämtliche Vorstellungen des Stadttheaters');
INSERT INTO TOURISTENATTRAKTION (Bezeichnung, Beschreibung) VALUES ('Museum', 'Freier Eintritt in sämtliche Museen der Umgebung');

COMMIT;

INSERT INTO IN_UMGEBUNG (Bezeichnung, Wohnungs_Name, Entfernung) VALUES ('Museum', 'El Barto', 56);
INSERT INTO IN_UMGEBUNG (Bezeichnung, Wohnungs_Name, Entfernung) VALUES ('Tauchen', 'El Torro', 36.12);
INSERT INTO IN_UMGEBUNG (Bezeichnung, Wohnungs_Name, Entfernung) VALUES ('Theater', 'Gasthof Engel', 56);

COMMIT;

INSERT INTO BILD (Link, Wohnungs_Name) VALUES ('https://www.123lökj.de/Bild1', 'Gasthof Engel');
INSERT INTO BILD (Link, Wohnungs_Name) VALUES ('https://www.13kj.de/bild25', 'Gasthof Teufel');
INSERT INTO BILD (Link, Wohnungs_Name) VALUES ('https://www.1kj.de/Bild09', 'Gasthof Engel');
INSERT INTO BILD (Link, Wohnungs_Name) VALUES ('https://www.1sdf23j.de/bild34', 'Gasthof Engel');
INSERT INTO BILD (Link, Wohnungs_Name) VALUES ('https://www.123adsfkj.de/bild48', 'Gasthof Engel');

COMMIT;

INSERT INTO BUCHUNG (Wohnungs_Name, StartDatum, EndDatum, RechnungsNr, BuchungsDatum, Kundenmail, Endpreis)
VALUES ('Hotel Rizzi', TO_DATE('14.03.2014', 'DD.MM.YYYY'), TO_DATE('21.03.2014', 'DD.MM.YYYY'), 489834, TO_DATE('31.12.2013', 'DD.MM.YYYY'), 'Karl@Sauter.de', 2099.93);
INSERT INTO BUCHUNG (Wohnungs_Name, StartDatum, EndDatum, RechnungsNr, BuchungsDatum, Kundenmail, Endpreis)
VALUES ('El Torro', TO_DATE('24.09.2015', 'DD.MM.YYYY'), TO_DATE('24.10.2015', 'DD.MM.YYYY'), 123456, TO_DATE('15.06.2014', 'DD.MM.YYYY'), 'Max@Muster.de', 5045.13);
INSERT INTO BUCHUNG (Wohnungs_Name, StartDatum, EndDatum, RechnungsNr, BuchungsDatum, Kundenmail, Endpreis)
VALUES ('Hotel Rizzi', TO_DATE('22.03.2014', 'DD.MM.YYYY'), TO_DATE('14.04.2014', 'DD.MM.YYYY'), 123456, TO_DATE('03.01.2013', 'DD.MM.YYYY'), 'Max@Muster.de', 3456.34);
INSERT INTO BUCHUNG (Wohnungs_Name, StartDatum, EndDatum, RechnungsNr, BuchungsDatum, Kundenmail, Endpreis)
VALUES ('El Barto', TO_DATE('18.06.2015', 'DD.MM.YYYY'), TO_DATE('18.06.2016', 'DD.MM.YYYY'), 123456, TO_DATE('10.01.2014', 'DD.MM.YYYY'), 'Gundula@Gans.de', 185099.93);
INSERT INTO BUCHUNG (Wohnungs_Name, StartDatum, EndDatum, RechnungsNr, BuchungsDatum, Kundenmail, Endpreis)
VALUES ('Da Pizzo', TO_DATE('23.03.2018', 'DD.MM.YYYY'), TO_DATE('26.04.2018', 'DD.MM.YYYY'), 234568, TO_DATE('03.01.2017', 'DD.MM.YYYY'), 'italienlover@hotmail.com', 2345.44);
INSERT INTO BUCHUNG (Wohnungs_Name, StartDatum, EndDatum, RechnungsNr, BuchungsDatum, Kundenmail, Endpreis)
VALUES ('El Barto', TO_DATE('05.11.2012', 'DD.MM.YYYY'), TO_DATE('12.11.2012', 'DD.MM.YYYY'), 234568, TO_DATE('03.01.2010', 'DD.MM.YYYY'), 'italienlover@hotmail.com', 82000.99);
COMMIT;

GRANT SELECT, INSERT ON KUNDE to dbsys54;
GRANT SELECT, INSERT on BUCHUNG to dbsys54;
GRANT SELECT on LAND to dbsys54;
GRANT SELECT on FERIENWOHNUNG to dbsys54;
GRANT SELECT on Bild to dbsys54;
GRANT SELECT on Ausstattung to dbsys54;
GRANT SELECT on ausgestattet_in to dbsys54;
GRANT SELECT on Touristenattraktion to dbsys54;
GRANT SELECT on in_umgebung to dbsys54;
