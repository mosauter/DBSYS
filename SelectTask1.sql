SELECT * 
FROM dbsys37.Ferienwohnung
WHERE WOHNUNGS_NAME NOT IN (SELECT WOHNUNGS_NAME FROM dbsys37.Buchung WHERE (STARTDATUM BETWEEN TO_DATE('01.11.2012', 'DD.MM.YYYY') AND TO_DATE('21.11.2012', 'DD.MM.YYYY')) OR
(ENDDATUM BETWEEN TO_DATE('01.11.2012', 'DD.MM.YYYY') AND TO_DATE('21.11.2012', 'DD.MM.YYYY'))) AND
WOHNUNGS_NAME IN (SELECT WOHNUNGS_NAME FROM dbsys37.ausgestattet_in WHERE BEZEICHNUNG LIKE 'Sauna') AND 
WOHNUNGS_NAME  IN (SELECT WOHNUNGS_NAME FROM dbsys37.ferienwohnung WHERE LAND_NAME Like 'Spanien');



SELECT dbsys37.ausgestattet_in.Wohnungs_Name
From dbsys37.ausgestattet_in
GROUP BY Wohnungs_Name
Having COUNT(dbsys37.ausgestattet_in.Bezeichnung) = (SELECT MAX(ausgestattet.Anzahl)
FROM (SELECT WOHNUNGS_NAME, COUNT(Bezeichnung) as Anzahl FROM dbsys37.ausgestattet_in GROUP BY dbsys37.ausgestattet_in.WOHNUNGS_NAME) ausgestattet);


SELECT land.LAND_NAME, COUNT(kuenftigebuchung.Startdatum) as ANZAHL
FROM dbsys37.land LEFT OUTER JOIN dbsys37.Ferienwohnung wohn ON (land.LAND_NAME = wohn.LAND_NAME)
LEFT OUTER JOIN (SELECT WOHNUNGS_NAME, Startdatum FROM dbsys37.buchung WHERE Startdatum > 
(SELECT TO_CHAR(SYSDATE, 'DD.MM.YYYY') FROM DUAL)) kuenftigebuchung ON (wohn.WOHNUNGS_NAME = kuenftigebuchung.WOHNUNGS_NAME)
GROUP BY land.LAND_NAME
ORDER BY ANZAHL DESC;