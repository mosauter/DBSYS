// DBSYS.java

package dbsys;

import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;

/**
 * DBSYS is the java task for the DBSYS-lecture.
 * @author Moritz Sauter (SauterMoritz@gmx.de)
 * @version 1.00
 * @since 2015-01-13
 */
public final class DBSYS {

    /**
     * Private constructor.
     */
    private DBSYS() { }

    /**
     * Scanner from console.
     */
    private static final Scanner INPUT = new Scanner(System.in);

    /**
     * @param args the command line arguments
     * @throws java.lang.ClassNotFoundException jdbc exception
     * @throws java.sql.SQLException jdbc exception
     */
    public static void main(final String[] args)
            throws ClassNotFoundException, SQLException {
        System.out.println("Do you want to search a flat or do you want to "
                + "book a flat? [ search | book ]");
        boolean search = INPUT.next().equals("search");
        String sqlQuery;
        if (search) {
            sqlQuery = flatSearch();
        } else {
            sqlQuery = bookFlat();
        }

        Class.forName("oracle.jdbc.driver.OracleDriver");
        String url = "jdbc:oracle:thin:@oracle12c.in."
                + "htwg-konstanz.de:1521:ora12c";
        try (Connection conn
                = DriverManager.getConnection(url, "dbsys35", "dbsys35");
                Statement stmt = conn.createStatement();
                ResultSet rs
                = stmt.executeQuery(sqlQuery)) {

            while (rs.next()) {
                String r1 = rs.getString("name");
                int r2 = rs.getInt("gehalt");
                System.out.println(r1 + " verdient " + r2);
            }
        }
    }

    /**
     * Static utility method to search a flat.
     * @return a sql query to search a flat with the specified values
     */
    private static String flatSearch() {
        System.out.println("In which Land you want to search flats?");
        String land = INPUT.next();

        System.out.println("How many rooms you want to have at least? Give the "
                + "Number as an Integer");
        int rooms = INPUT.nextInt();

        System.out.println("On which date you want to start your holidays? "
                + "Format: YYYY-MM-DD");
        Date startDate = Date.valueOf(INPUT.nextLine().trim());

        System.out.println("On which date you want to return? "
                + "Format: YYYY-MM-DD");
        Date endDate = Date.valueOf(INPUT.nextLine().trim());

        System.out.println("Do you want to add an optional equipment? (y / n)");
        boolean equip = INPUT.next().equals("y");
        String equipment = "";

        if (equip) {
            System.out.println("What equipment do you want to add?");
            equipment = INPUT.nextLine().trim();
        }
        String query = "";
        return query;
    }

    /**
     * Static utility method to book a flat.
     * @return a sql query to book a flat with the specified values.
     */
    private static String bookFlat() {
        System.out.println("Type in your Mail which identifies you at "
                + "our service:");
        String eMail = INPUT.next();

        System.out.println("Type in the name of the specified flat:");
        String flatName = INPUT.nextLine().trim();

        System.out.println("Type in your start date (Format: YYYY-MM-DD):");
        Date startDate = Date.valueOf(INPUT.nextLine().trim());

        System.out.println("Type in your return date (Format: YYYY-MM-DD):");
        Date endDate = Date.valueOf(INPUT.nextLine().trim());

        String query = "";
        return query;
    }
}
