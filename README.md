DBSYS
=====

Repository for the Database - Systems - Class. 

The Task is to create a Database for a Tourist Office with several holiday appartments.

The Tasks are:
* *Task 1*
  * Create the Tables
  * Fill the Tables with some flats in different countries
  * to frame some Select-Inquiries

* *Task 2*
  * Create a trigger which copies a storiette booking in a specified table
  * Create a view in which you can fast check how many bookings a specified customer have
